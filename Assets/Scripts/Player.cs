using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Player : MonoBehaviour
{
    public WayPoints waypoints;
    [SerializeField] float speed;
    public int waypointIndex;
    [SerializeField] List<GameObject> oder;
    [SerializeField] List<GameObject> oderImage;
    [SerializeField] List<GameObject> newOderImage;
    [SerializeField] List<GameObject> newOder = new List<GameObject>();
    public int count;
    [SerializeField] GameObject RayPos;
    [SerializeField] LayerMask layerMask;
    bool moveToPosition;
    [SerializeField] GameObject parentImage;
    PlayerTimer timer;
    List<T> GetRandomElements<T>(List<T> inputList, int count)
    {
        List<T> outputList = new List<T>();
        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, inputList.Count);
            outputList.Add(inputList[index]);
        }
        return outputList;
    }

    void Start()
    {
        timer = GetComponent<PlayerTimer>();
        int listSize = Random.Range(1, 4);
        newOder = GetRandomElements(oder, listSize);
        int[] oderImageIds = new int[newOder.Count];
        for (int i = 0; i < listSize; i++)
        {
            oderImageIds[i] = newOder[i].GetComponent<ItemID>().Id;
            GameObject obj = Instantiate(oderImage[oderImageIds[i]], parentImage.transform.position, parentImage.transform.rotation);
            obj.transform.SetParent(parentImage.transform);
        }
        waypoints = FindObjectOfType<WayPoints>();
        Check();
        moveToPosition = true;
    }

    public void GetFieldFree(bool free)
    {
        waypoints.points[waypointIndex].GetComponent<Point>().isFree = free;
    }

    void Update()
    {
        
        if (moveToPosition)
        {
            //���� ������� ��������
            if (waypoints.points[waypointIndex].GetComponent<Point>().isFree)
            {
                transform.position = Vector3.MoveTowards(transform.position, waypoints.points[waypointIndex].transform.position, speed * Time.deltaTime);
            }
            //������
            else
            {
                Debug.Log("�� �����!");
                Check();
            }


            if (transform.position == waypoints.points[waypointIndex].transform.position)
            {
                waypoints.points[waypointIndex].GetComponent<Point>().isFree = false;
                transform.Rotate(0, 90 * 1f, 0);
                speed = 0;
                timer.start = true;
                moveToPosition = false;
                return;
            }
        }

        if (Physics.Raycast(RayPos.transform.position, transform.TransformDirection(Vector3.forward), out RaycastHit hitinfo, 20f, layerMask))
        {
            Debug.DrawRay(RayPos.transform.position, transform.TransformDirection(Vector3.forward) * hitinfo.distance, Color.red);
            Debug.Log(hitinfo.transform.name);
            //Debug.Log("Hit Something");

            //�������� ���� �������
            if ((layerMask.value & (1 << hitinfo.transform.gameObject.layer)) > 0)
            {
                //������ id ������
                int[] playerOderIds = new int[newOder.Count];
                for (int i = 0; i < newOder.Count; i++)
                {
                    playerOderIds[i] = newOder[i].GetComponent<ItemID>().Id;
                }

                //������ id �������
                List<GameObject> trayItems = hitinfo.transform.gameObject.GetComponent<Tray>().trayItems;
                int[] trayItemsIds = new int[trayItems.Count];
                for (int i = 0; i < trayItems.Count; i++)
                {
                    trayItemsIds[i] = trayItems[i].GetComponent<ItemID>().Id;
                }

                //����������
                System.Array.Sort(playerOderIds);
                System.Array.Sort(trayItemsIds);

                //���������    
                Debug.Log(playerOderIds.SequenceEqual(trayItemsIds));

                if (playerOderIds.SequenceEqual(trayItemsIds))
                {
                    Destroy(hitinfo.transform.gameObject);
                    GameManager.instance.AddScore();

                    waypoints.points[waypointIndex].GetComponent<Point>().isFree = true;
                    StartCoroutine(GoAway());
                }
                else
                {
                    GameManager.instance.TakeScore();
                    waypoints.points[waypointIndex].GetComponent<Point>().isFree = true;

                    Destroy(gameObject);
                }
            }
        }
    }

    void Check()
    {
        for (int p = 0; p < waypoints.points.Length; p++)
        {
            if (waypoints.points[p].GetComponent<Point>().isFree)
            {
                waypointIndex = p;
                return;
            }
        }
    }

    IEnumerator GoAway()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTimer : MonoBehaviour
{
    [SerializeField] Image timerBar;
    [SerializeField] float maxTime;
    public float timeLeft;
    public bool start;
    [SerializeField] GameObject bar;
    public bool timeLow;
    void Start()
    {
        timeLow = false;
        bar.SetActive(false);
        start = false;
        maxTime = Random.Range(5, 10);
        timeLeft = maxTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            bar.SetActive(true);
            if (timeLeft > 0)
            {
                timeLeft -= Time.deltaTime;
                timerBar.fillAmount = timeLeft / maxTime;
            }
            else
            {
                //StartCoroutine(destroy());
                GetComponent<Player>().GetFieldFree(true);
                GameManager.instance.TakeScore();                
                Destroy(gameObject);
            }
        }
    }
    IEnumerator destroy()
    {
        timeLow = true;
        yield return new WaitForSeconds(0.1f);      
        GameManager.instance.TakeScore();
        Destroy(gameObject);
    }
}

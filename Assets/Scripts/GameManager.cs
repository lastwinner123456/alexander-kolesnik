using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;
    [SerializeField] Text scoreText;
    [SerializeField] Text finaleScore;
    int score = 0;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] WayPoints waypoints;
    List<GameObject> players = new List<GameObject>();
    bool canSpawn=true;
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }      
        SpawnPlayer();
        InvokeRepeating("SpawnPlayer", 5f, 5f);
    }
    // Update is called once per frame
    void Update()
    {
        finaleScore.text = "Score :" + score;
        scoreText.text = "Score :" + score;       
    }
    public void AddScore()
    {
        score += 1;
    }
    public void TakeScore()
    {
        score -= 1;
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void SpawnPlayer()
    {
        int random = Random.RandomRange(0, 2);
        GameObject player = Instantiate(playerPrefab, waypoints.startPoint[random].transform.position, waypoints.startPoint[random].transform.rotation);
      //  players.Add(player);
    }
    //public void RemoveArray(GameObject client)
    //{
    //    players.Remove(client);
    //}

    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] float timeStart;
    [SerializeField] Text textTimer;
    [SerializeField] GameObject completePanel;
    bool open;
    private void Start()
    {
        Time.timeScale = 1;
        timeStart = 60;
    }
    void Update()
    {
        float t = timeStart - Time.timeSinceLevelLoad;
        string minute = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("F0");

        textTimer.text ="Time:"+  minute + ":" + seconds;
        if (t <= 0)
        {
         
            completePanel.SetActive(true);
            Time.timeScale = 0;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            open = !open;
        }
        if (open)
        {
            
            completePanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            completePanel.SetActive(false);
          
        }
    }
    
}

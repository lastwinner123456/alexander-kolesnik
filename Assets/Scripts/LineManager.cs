using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineManager : MonoBehaviour
{
    [SerializeField] GameObject[] items;
    [SerializeField] List<GameObject> itemsCount;
    [SerializeField] GameObject[] ItemsPositon;
    int itemIndex;
    int posIndex;
    [SerializeField] GameObject trayPos;
    [SerializeField] GameObject tray;
    [SerializeField] GameObject traySpawnPos;
    GameObject trayObj;

    void Start()
    {
        StartCoroutine(SpawnTray());            
        posIndex = 0;
    }

    public void ChooseImage(int index)
    {
        if (itemsCount.Count < 3 && posIndex < 3)
        {
            GameObject obj= Instantiate(items[index], ItemsPositon[posIndex].transform.position, ItemsPositon[posIndex].transform.rotation);
            obj.transform.SetParent(trayObj.transform);
            posIndex++;
            itemsCount.Add(items[index]);
            FindObjectOfType<Tray>().trayItems.Add(items[index]);
        }
        
    }
    IEnumerator MoveTray()
    {
        yield return new WaitForSeconds(0.2f);
        trayObj.transform.position = Vector3.MoveTowards(trayObj.transform.position, trayPos.transform.position, 1);
        StartCoroutine(SpawnTray());
    }
    IEnumerator SpawnTray()
    {
        yield return new WaitForSeconds(0.01f);
        posIndex = 0;
        trayObj =  Instantiate(tray, traySpawnPos.transform.position, traySpawnPos.transform.rotation);
       
    }
   
    public void OkButton()
    {
        
        itemsCount.Clear();
        StartCoroutine(MoveTray());
    }
}
